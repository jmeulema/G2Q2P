/* 
 * File:   UAPN.hpp
 * Author: julienmeulemans
 *
 * Created on March 25, 2014, 12:14 PM
 */

#ifndef UAPN_HPP
#define	UAPN_HPP
#include "QPN.hpp"
#include "QDAS.hpp"

/*
 * Concrete under-approximation of a QDAS into a PetriNet
 *  by abstracting serial queues with finite waiting queues,
 *  and all dispatches by asynchronous ones.
 */
class UAPN : public QPN {
public:
    UAPN(unsigned, QDAS *, unsigned);
protected:
    virtual std::string getHeaderText();
    const unsigned queueSize;
    virtual std::string visit(QDAS::DispatchAction *, std::string, std::string);
    std::string getQueueWaitingSlotPlace(std::string, unsigned, bool);
    std::string getBlockWaitingSlotPlace(std::string, std::string, unsigned);
    virtual void visitQueue(const QDAS::Queue *);
    virtual std::string getBlockVersionNameOnSQ(std::string, std::string);
    virtual std::string createEndOfBlockOnSQ(std::string, std::string);
    virtual void createBlockSchedulingOnSQ(std::string, std::string, std::string, std::string);
};

#endif	/* UAPN_HPP */

