/* 
 * File:   OAPN.hpp
 * Author: julienmeulemans
 *
 * Created on March 16, 2014, 11:11 AM
 */

#ifndef OAPN_HPP
#define	OAPN_HPP

#include <list>
#include "QPN.hpp"
#include "QDAS.hpp"


/*
 * Concrete over-approximation of a QDAS into a PetriNet
 *  by abstracting all queues as concurrent ones 
 *  and all dispatches by asynchronous ones.
 */
class OAPN : public QPN {
public:
    OAPN(unsigned, QDAS *);
protected:
    virtual std::string getHeaderText();
    virtual std::string getBlockVersionNameOnSQ(std::string, std::string);
    virtual std::string createEndOfBlockOnSQ(std::string, std::string);
    virtual void createBlockSchedulingOnSQ(std::string, std::string, std::string, std::string);
};

#endif	/* OAPN_HPP */

