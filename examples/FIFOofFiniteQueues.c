#include <dispatch/dispatch.h>
#include "../g2q2p.h"


 /* 
    Program used to show that the finite queue of 3 built by the under-approximation, 
    ensure mutual exclusion and FIFO ordering in the execution of the submitted blocks.
 */

int last_managed_block = 0;

int main(){
    dispatch_queue_t serialQueue = dispatch_queue_create("queue", DISPATCH_QUEUE_SERIAL);
        
    dispatch_block_t b1 = ^{  
        assert(last_managed_block == 0); 
        last_managed_block = 1; };
        
    dispatch_block_t b2 = ^{  
        assert(last_managed_block == 1); 
        last_managed_block = 2; };
            
    dispatch_block_t b3 = ^{  
        assert(last_managed_block == 2); };
        
    dispatch_async(serialQueue, b1);
    dispatch_async(serialQueue, b2);
    dispatch_async(serialQueue, b3);

    return 0;
}
