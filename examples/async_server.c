//#include <stdio.h>
#include <dispatch/dispatch.h>
//#include <stdlib.h>
#include "../g2q2p.h"


 /* 
     Implementation of a server that can accept two types of request: 
        - page rendering
        - file storage. 
     The storage system can only handle one request at the time.
     It needs to be able to serve multiple rendering requests at the same time.
     For business analysis pruposes, everything is logged on a single sequetially accessed file. 
 */


 int storage_system_busy = 0; 


int main(){
    dispatch_queue_t logQueue = dispatch_queue_create("logging file access queue", DISPATCH_QUEUE_SERIAL);
    dispatch_queue_t workQueue = dispatch_queue_create("concurrent work queue", DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_block_t logRequest = ^{  
        logData:
        /* write request meta data on the log */; };
        
    dispatch_block_t handleRendering = ^{  
        while(NONDETERMINISTIC){
            /* do some heavy rendering computations */
            dispatch_async(logQueue, logRequest);
        } };
        
    dispatch_block_t handleFileStorage = ^{  
        storeFile: /*  store file */
        dispatch_async(logQueue, logRequest);
        storage_system_busy = 0; };

    while(1){
        if(storage_system_busy == 0 && NONDETERMINISTIC){ 
            assert(storage_system_busy == 0); // check for possible race conditions
            storage_system_busy = 1;
            dispatch_async(workQueue, handleFileStorage);
        } else if(NONDETERMINISTIC){
            dispatch_async(workQueue, handleRendering);
        }
    }

    return 0;
}
