//#include <stdio.h>
#include <dispatch/dispatch.h>
//#include <stdlib.h>
#include "../g2q2p.h"


 /* 
    Implementation of a program interacting with the user in order to save an article on a database.
    An article is composed of:
        - a text part: written in real time by the user -> slow to produce, but fast to transmit
        - a video part: ready to be sent -> fast to produce, but slow to send
    In order to avoid long delay for the submission, the video is submitted first, and, during its uploading, the user is invited to write the corresponding text. 
 */

int active_connection = 0;

int main(){
    dispatch_queue_t dbQueue = dispatch_queue_create("database access queue", DISPATCH_QUEUE_SERIAL);
        
    dispatch_block_t disconnect = ^{  
        assert(active_connection == 1); // the connection is open
        /* close connection to the db */ 
        active_connection = 0; 
        /* notify the user of the end of the uploading */ };
        
    dispatch_block_t uploadVideo = ^{  
        assert(active_connection == 1); // the connection is open
        /* upload video */};
    
    dispatch_block_t uploadText = ^{  
        assert(active_connection == 1); // the connection is open
        /* upload text */};
        
    while(1){
        if(NONDETERMINISTIC && active_connection == 0){
            /* open connection to the db */ 
            active_connection = 1;
            /* ask the user for the video to upload */
            dispatch_async(dbQueue, uploadVideo);
    
            /* help the user writting its text with eddition options */
            dispatch_async(dbQueue, uploadText);
    
            dispatch_async(dbQueue, disconnect);
        } else {
            /* do something else */
        }
    }
    return 0;
}
