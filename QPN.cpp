/* 
 * File:   QPN.cpp
 * Author: julienmeulemans
 * 
 * Created on March 21, 2014, 6:10 PM
 */

#include <algorithm>

#include "QPN.hpp"
#include <iostream>

static void handleError(std::string s) {
    std::cout << "QPN construction ERROR: " << s << std::endl;
    exit(EXIT_FAILURE);
}

QPN::QPN(unsigned max, QDAS * q) : PN(), maxVarVal(max), qdas(q) {
    addPlace("ASSERT_FAIL", 0);
}

void QPN::build() {
    for (std::list<QDAS::Queue*>::const_iterator it = qdas->getQueues()->cbegin();
            it != qdas->getQueues()->cend(); ++it) {
        visitQueue(*it);
    }
    for (std::list<QDAS::Variable*>::const_iterator it = qdas->getVars()->cbegin();
            it != qdas->getVars()->cend(); ++it) {
        visitVariable(*it);
    }
    visitBlock(qdas->getMainBlock(), 0);
    qdas = NULL;
    convertedBlocks.clear();
}

void QPN::visitQueue(const QDAS::Queue * q){
}

std::string QPN::getPlaceOfVarForVal(std::string n, unsigned v) {
    return n + "_" + std::to_string(v);
}

std::string QPN::getUniqueScopedPlaceNameFrom(std::string n, std::string scope){
    return getUniquePlaceFrom(n) + "_FROM_" + scope;
}


void QPN::visitVariable(const QDAS::Variable * v) {
    std::string inv;
    if (v->initval > maxVarVal) {
        handleError("Variable initial variable too big :" + std::to_string(v->initval));
    }
    for (unsigned i = 0; i <= maxVarVal; ++i) {
        std::string place = getPlaceOfVarForVal(v->name, i);
        addPlace(place, ((i == v->initval) ? 1 : 0));
        if (i != 0) {
            inv += ", ";
        }
        inv += place + "=1";
    }
    addInvariant(inv);
}

void QPN::visitBlock(const QDAS::Block * b, const QDAS::Queue * q) {
    bool isMain = b == qdas->getMainBlock();
    std::string endPlace;
    std::string blockVersionName;
    if ( !isMain && q->isSerial()) {
        endPlace = createEndOfBlockOnSQ(b->name, q->name);
        blockVersionName = getBlockVersionNameOnSQ(b->name, q->name);
    } else {
        endPlace = createEndOfBlockOnCQ(b->name);
        blockVersionName = b->name;
    }
    addPlace(blockVersionName, (isMain ? 1 : 0));

    // build the actions of the block
    std::string beginPlace = visit(b->actions, endPlace, blockVersionName);
    // link the block's origin place to the core
    if (!isMain && q->isSerial()) {
        createBlockSchedulingOnSQ(blockVersionName, beginPlace, q->name, b->name);
    } else {
        createBlockSchedulingOnCQ(blockVersionName, beginPlace);
    }
    convertedBlocks.push_front(blockVersionName);
}


std::string QPN::visit(const std::list<QDAS::Action*>& actions, std::string continuationPlace, std::string scope) {
    for (std::list<QDAS::Action*>::const_reverse_iterator rit = actions.crbegin(); rit != actions.crend(); ++rit) {
        continuationPlace = (*rit)->accept(this, continuationPlace, scope);
    }
    return continuationPlace;
}

bool QPN::isBlockVersionAlreadyEncoded(std::string s) {
    return convertedBlocks.cend() != std::find(convertedBlocks.cbegin(), convertedBlocks.cend(), s);
}

std::string QPN::createEndOfBlockOnCQ(std::string blockName) {
    std::string endPlace = getUniqueScopedPlaceNameFrom("END", blockName);
    addPlace(endPlace, 0);
    Transition * trans = new Transition();
    trans->guards.push_back(endPlace);
    trans->decEffects.push_back(endPlace);
    addTransition(trans);
    return endPlace;
}

void QPN::createBlockSchedulingOnCQ(std::string startPlace, std::string beginPlace) {
    Transition * trans = new Transition();
    trans->guards.push_back(startPlace);
    trans->decEffects.push_back(startPlace);
    trans->incEffects.push_back(beginPlace);
    addTransition(trans);
}

std::string QPN::visit(QDAS::NullAction * a, std::string continuationPlace, std::string scope) {
    std::string startPlace;
    if (a->label.empty()) {
        startPlace = getUniqueScopedPlaceNameFrom("DO_NOTHING", scope);
    } else {
        startPlace = a->label + "_FROM_" + scope;
    }
    addPlace(startPlace, 0);
    Transition * trans = new Transition();
    trans->guards.push_back(startPlace);
    trans->decEffects.push_back(startPlace);
    trans->incEffects.push_back(continuationPlace);
    addTransition(trans);
    return startPlace;
}

std::string QPN::visit(QDAS::DispatchAction * a, std::string continuationPlace, std::string scope) {
    std::string startPlace;
    if (a->label.empty()) {
        startPlace = getUniqueScopedPlaceNameFrom("DISP", scope);
    } else {
        startPlace = a->label + "_FROM_" + scope;
    }
    std::string blockVersion;
    const QDAS::Queue * q = qdas->getQueueNamed(a->queue);
    if (q->isSerial()) {
        blockVersion = getBlockVersionNameOnSQ(a->block, a->queue);
    } else {
        blockVersion = a->block;
    }

    if (!isBlockVersionAlreadyEncoded(blockVersion)) {
        visitBlock(qdas->getBlockNamed(a->block), q);
    }

    addPlace(startPlace, 0);
    Transition * trans = new Transition();
    trans->guards.push_back(startPlace);
    trans->decEffects.push_back(startPlace);
    trans->incEffects.push_back(blockVersion);
    trans->incEffects.push_back(continuationPlace);
    addTransition(trans);
    return startPlace;
}

std::string QPN::visit(QDAS::DispatchAsyncAction * a, std::string continuationPlace, std::string scope) {
    return visit((QDAS::DispatchAction *) a, continuationPlace, scope);
}

std::string QPN::visit(QDAS::DispatchSyncAction * a, std::string continuationPlace, std::string scope) {
    return visit((QDAS::DispatchAction *) a, continuationPlace, scope);
}

std::string QPN::visit(QDAS::AssertAction * a, std::string continuationPlace, std::string scope) {
    std::string startPlace;
    if (a->label.empty()) {
        startPlace = getUniqueScopedPlaceNameFrom("ASSERT", scope);
    } else {
        startPlace = a->label + "_FROM_" + scope;
    }
    addPlace(startPlace, 0);

    a->cond->accept(this, startPlace, continuationPlace, "ASSERT_FAIL", scope);
    return startPlace;
}

std::string QPN::visit(QDAS::IfAction * a, std::string continuationPlace, std::string scope) {
    if (a->cons.empty()){
        a->cons.push_front(new QDAS::NullAction());
    }
    if (a->alt.empty()) {
        a->alt.push_front(new QDAS::NullAction());
    }
    std::string startPlace;
    if (a->label.empty()) {
        startPlace = getUniqueScopedPlaceNameFrom("IF", scope);
    } else {
        startPlace = a->label + "_FROM_" + scope;
    }
    addPlace(startPlace, 0);
    
    std::string consPlace = visit(a->cons, continuationPlace, scope);
    std::string altPlace = visit(a->alt, continuationPlace, scope);

    a->cond->accept(this, startPlace, consPlace, altPlace, scope);
    return startPlace;
}

std::string QPN::visit(QDAS::WhileAction * a, std::string continuationPlace, std::string scope) {
    if (a->core.empty()){
        a->core.push_front(new QDAS::NullAction());
    }
    std::string startPlace;
    if (a->label.empty()) {
        startPlace = getUniqueScopedPlaceNameFrom("WHILE", scope);
    } else {
        startPlace = a->label + "_FROM_" + scope;
    }
    addPlace(startPlace, 0);
    
    std::string corePlace = visit(a->core, startPlace, scope);
    a->cond->accept(this, startPlace, corePlace, continuationPlace, scope);
    return startPlace;
}

void QPN::createEqualityTest(std::string startPlace, std::string trueContinuationPlace, std::string falseContinuationPlace, std::string var, unsigned val) {
    for (int i = 0; i <= maxVarVal; ++i) {
        Transition * trans = new Transition();
        trans->guards.push_back(startPlace);
        trans->guards.push_back(getPlaceOfVarForVal(var, i));
        if (i == val) {
            trans->incEffects.push_back(trueContinuationPlace);
        } else {
            trans->incEffects.push_back(falseContinuationPlace);
        }
        trans->decEffects.push_back(startPlace);
        addTransition(trans);
    }
}

void QPN::visit(QDAS::EqCond * c, std::string startPlace, std::string trueContinuationPlace, std::string falseContinuationPlace, std::string scope) {
    return createEqualityTest(startPlace, trueContinuationPlace, falseContinuationPlace, c->variable, c->literal);
}

void QPN::visit(QDAS::NeqCond * c, std::string startPlace, std::string trueContinuationPlace, std::string falseContinuationPlace, std::string scope) {
    return createEqualityTest(startPlace, falseContinuationPlace, trueContinuationPlace, c->variable, c->literal);
}

void QPN::visit(QDAS::AlwaysTrueCond * c, std::string startPlace, std::string trueContinuationPlace, std::string falseContinuationPlace, std::string scope) {
    Transition * trans = new Transition();
    trans->guards.push_back(startPlace);
    trans->decEffects.push_back(startPlace);
    trans->incEffects.push_back(trueContinuationPlace);
    addTransition(trans);
}

void QPN::visit(QDAS::NondeterministicCond * c, std::string startPlace, std::string trueContinuationPlace, std::string falseContinuationPlace, std::string scope) {
    Transition * trans = new Transition();
    trans->guards.push_back(startPlace);
    trans->decEffects.push_back(startPlace);
    trans->incEffects.push_back(trueContinuationPlace);
    addTransition(trans);
    
    trans = new Transition();
    trans->guards.push_back(startPlace);
    trans->decEffects.push_back(startPlace);
    trans->incEffects.push_back(falseContinuationPlace);
    addTransition(trans);
}

void QPN::visit(QDAS::ANDCond * c, std::string startPlace, std::string trueContinuationPlace, std::string falseContinuationPlace, std::string scope) {
    std::string tmpPlace = getUniqueScopedPlaceNameFrom("TMP", scope);
    addPlace(tmpPlace, 0);
    
    c->left->accept(this, startPlace, tmpPlace, falseContinuationPlace, scope);
    c->right->accept(this, tmpPlace, trueContinuationPlace, falseContinuationPlace, scope);
}

void QPN::visit(QDAS::ORCond * c, std::string startPlace, std::string trueContinuationPlace, std::string falseContinuationPlace, std::string scope) {
    std::string tmpPlace = getUniqueScopedPlaceNameFrom("TMP", scope);
    addPlace(tmpPlace, 0);
    
    c->left->accept(this, startPlace, trueContinuationPlace, tmpPlace, scope);
    c->right->accept(this, tmpPlace, trueContinuationPlace, falseContinuationPlace, scope);
}

std::string QPN::visit(QDAS::AssignFromLitAction * a, std::string continuationPlace, std::string scope) {
    std::string startPlace;
    if (a->valOrg > maxVarVal) {
        handleError("Assignment from literal too big :" + std::to_string(a->valOrg));
    }
    if (a->label.empty()) {
        startPlace = getUniqueScopedPlaceNameFrom("ASSIGN", scope);
    } else {
        startPlace = a->label + "_FROM_" + scope;
    }
    addPlace(startPlace, 0);

    // if the variable already has the good value
    Transition * trans = new Transition();
    trans->guards.push_back(startPlace);
    trans->guards.push_back(getPlaceOfVarForVal(a->dest, a->valOrg));
    trans->incEffects.push_back(continuationPlace);
    trans->decEffects.push_back(startPlace);
    addTransition(trans);

    // if the variable doesn't have the good value
    for (unsigned i = 0; i <= maxVarVal; ++i) {
        if (i != a->valOrg) {
            trans = new Transition();
            trans->guards.push_back(startPlace);
            trans->guards.push_back(getPlaceOfVarForVal(a->dest, i));
            trans->incEffects.push_back(continuationPlace);
            trans->decEffects.push_back(startPlace);
            trans->decEffects.push_back(getPlaceOfVarForVal(a->dest, i));
            trans->incEffects.push_back(getPlaceOfVarForVal(a->dest, a->valOrg));
            addTransition(trans);
        }
    }
    return startPlace;
}

std::string QPN::visit(QDAS::AssignFromVarAction * a, std::string continuationPlace, std::string scope) {
    std::string startPlace;
    if (a->label.empty()) {
        startPlace = getUniqueScopedPlaceNameFrom("ASSIGN", scope);
    } else {
        startPlace = a->label + "_FROM_" + scope;
    }
    addPlace(startPlace, 0);

    Transition * trans;
    for (unsigned i = 0; i <= maxVarVal; ++i) { // orig
        for (unsigned j = 0; j <= maxVarVal; ++j) { // dest
            if (i == j) {
                // if the variables already have the same value
                trans = new Transition();
                trans->guards.push_back(startPlace);
                trans->guards.push_back(getPlaceOfVarForVal(a->varOrg, i));
                trans->guards.push_back(getPlaceOfVarForVal(a->dest, j));
                trans->incEffects.push_back(continuationPlace);
                trans->decEffects.push_back(startPlace);
                addTransition(trans);
            } else {
                // if the variables have different values
                trans = new Transition();
                trans->guards.push_back(startPlace);
                trans->guards.push_back(getPlaceOfVarForVal(a->varOrg, i));
                trans->guards.push_back(getPlaceOfVarForVal(a->dest, j));
                trans->incEffects.push_back(continuationPlace);
                trans->decEffects.push_back(startPlace);
                trans->decEffects.push_back(getPlaceOfVarForVal(a->dest, j));
                trans->incEffects.push_back(getPlaceOfVarForVal(a->dest, i));
                addTransition(trans);
            }
        }
    }
    return startPlace;
}