/* 
 * File:   OAPN.cpp
 * Author: julienmeulemans
 * 
 * Created on March 16, 2014, 11:11 AM
 */

#include "OAPN.hpp"

OAPN::OAPN(unsigned max, QDAS* q) : QPN(max, q) {
}

std::string OAPN::getHeaderText(){
    return "Over approximation of a QDAS, with variables bounded to [0," + std::to_string(maxVarVal) + "].";
}


std::string OAPN::getBlockVersionNameOnSQ(std::string blockName, std::string queueName) {
    return blockName;
}

std::string OAPN::createEndOfBlockOnSQ(std::string blockName, std::string queueName) {
    return createEndOfBlockOnCQ(blockName);
}

void OAPN::createBlockSchedulingOnSQ(std::string startPlace, std::string beginPlace, std::string queueName, std::string blockName) {
    createBlockSchedulingOnCQ(startPlace, beginPlace);
}
