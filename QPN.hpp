/* 
 * File:   QPN.hpp
 * Author: julienmeulemans
 *
 * Created on March 21, 2014, 6:10 PM
 */

#ifndef QPN_HPP
#define	QPN_HPP

#include <list>
#include <string>
#include "PN.hpp"
#include "QDAS.hpp"

/*
 * Abstract approximation of a QDAS into a PetriNet
 *  by abstracting all dispatches by asynchronous ones.
 *  Management of serial queues are left to be defined 
 *  by subclasses.
 */
class QPN : public PN, public QDAS::ActionVisitor, public QDAS::ConditionVisitor {
public:
    QPN(unsigned, QDAS*);
    void build();
protected:
    static std::string getPlaceOfVarForVal(std::string, unsigned);
    std::string getUniqueScopedPlaceNameFrom(std::string, std::string);
    virtual std::string visit(QDAS::NullAction *, std::string, std::string);
    virtual std::string visit(QDAS::DispatchAsyncAction *, std::string, std::string);
    virtual std::string visit(QDAS::DispatchSyncAction *, std::string, std::string);
    virtual std::string visit(QDAS::AssertAction *, std::string, std::string);
    virtual std::string visit(QDAS::IfAction *, std::string, std::string);
    virtual std::string visit(QDAS::WhileAction *, std::string, std::string);
    virtual std::string visit(QDAS::AssignFromLitAction *, std::string, std::string);
    virtual std::string visit(QDAS::AssignFromVarAction *, std::string, std::string);
    virtual std::string visit(QDAS::DispatchAction *, std::string, std::string);
    std::string visit(const std::list<QDAS::Action*>&, std::string, std::string);
    virtual void visitQueue(const QDAS::Queue *);
    void visitVariable(const QDAS::Variable *);
    void visitBlock(const QDAS::Block *, const QDAS::Queue *);
    bool isBlockVersionAlreadyEncoded(std::string);
    virtual std::string getBlockVersionNameOnSQ(std::string, std::string) = 0;
    virtual std::string createEndOfBlockOnSQ(std::string, std::string) = 0;
    virtual std::string createEndOfBlockOnCQ(std::string);
    virtual void createBlockSchedulingOnSQ(std::string, std::string, std::string, std::string) = 0;
    virtual void createBlockSchedulingOnCQ(std::string, std::string);
    void createEqualityTest(std::string, std::string, std::string, std::string, unsigned);
    void visit(QDAS::AlwaysTrueCond *, std::string, std::string, std::string, std::string);
    void visit(QDAS::NondeterministicCond *, std::string, std::string, std::string, std::string);
    void visit(QDAS::EqCond *, std::string, std::string, std::string, std::string);
    void visit(QDAS::NeqCond *, std::string, std::string, std::string, std::string);
    void visit(QDAS::ANDCond *, std::string, std::string, std::string, std::string);
    void visit(QDAS::ORCond *, std::string, std::string, std::string, std::string);
    void createANDTest(std::string, std::string, std::string, std::string, std::string, std::string);
    const unsigned maxVarVal;
    QDAS * qdas;
    std::list<std::string> convertedBlocks;
};

#endif	/* QPN_HPP */

