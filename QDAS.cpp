/* 
 * File:   QDAS.cpp
 * Author: julienmeulemans
 * 
 * Created on March 15, 2014, 1:35 PM
 */

#include "QDAS.hpp"
#include <iostream>

void QDAS::AlwaysTrueCond::accept(ConditionVisitor * c, std::string startPlace, std::string trueContinuation, std::string falseContinuation, std::string scope) {
    c->visit(this, startPlace, trueContinuation, falseContinuation, scope);
}

void QDAS::NondeterministicCond::accept(ConditionVisitor * c, std::string startPlace, std::string trueContinuation, std::string falseContinuation, std::string scope) {
    c->visit(this, startPlace, trueContinuation, falseContinuation, scope);
}

void QDAS::ANDCond::accept(ConditionVisitor * c, std::string startPlace, std::string trueContinuation, std::string falseContinuation, std::string scope) {
    c->visit(this, startPlace, trueContinuation, falseContinuation, scope);
}

void QDAS::ORCond::accept(ConditionVisitor * c, std::string startPlace, std::string trueContinuation, std::string falseContinuation, std::string scope) {
    c->visit(this, startPlace, trueContinuation, falseContinuation, scope);
}

void QDAS::EqCond::accept(ConditionVisitor * c, std::string startPlace, std::string trueContinuation, std::string falseContinuation, std::string scope) {
    c->visit(this, startPlace, trueContinuation, falseContinuation, scope);
}

void QDAS::NeqCond::accept(ConditionVisitor * c, std::string startPlace, std::string trueContinuation, std::string falseContinuation, std::string scope) {
    c->visit(this, startPlace, trueContinuation, falseContinuation, scope);
}

void QDAS::addQueue(Queue * q) {
    queues.push_front(q);
}

void QDAS::addBlock(Block* b) {
    blocks.push_front(b);
}

void QDAS::setMainBlock(Block* b) {
    mainBlock = b;
}

void QDAS::addVariable(Variable* v) {
    vars.push_front(v);
}

const QDAS::Queue * QDAS::getQueueNamed(std::string name) {
    const std::list<QDAS::Queue*>::const_iterator qit = std::find_if(
            queues.cbegin(),
            queues.cend(),
            [ = ](QDAS::Queue * q){
        return q->name == name;
    });
    return (*qit);
}

const QDAS::Block * QDAS::getBlockNamed(std::string name) {
    const std::list<QDAS::Block*>::const_iterator bit = std::find_if(
            blocks.cbegin(),
            blocks.cend(),
            [ = ](QDAS::Block * b){
        return b->name == name;
    });
    return (*bit);
}

const std::list<QDAS::Queue*>* QDAS::getQueues() const {
    return &queues;
}

const std::list<QDAS::Block*>* QDAS::getBlocks() const {
    return &blocks;
}

const std::list<QDAS::Variable*>* QDAS::getVars() const {
    return &vars;
}

const QDAS::Block * QDAS::getMainBlock() const {
    return mainBlock;
}

std::string QDAS::NullAction::accept(ActionVisitor * v, std::string s, std::string scope) {
    return v->visit(this, s, scope);
}

std::string QDAS::DispatchAsyncAction::accept(ActionVisitor * v, std::string s, std::string scope) {
    return v->visit(this, s, scope);
}

std::string QDAS::DispatchSyncAction::accept(ActionVisitor * v, std::string s, std::string scope) {
    return v->visit(this, s, scope);
}

std::string QDAS::AssertAction::accept(ActionVisitor * v, std::string s, std::string scope) {
    return v->visit(this, s, scope);
}

std::string QDAS::IfAction::accept(ActionVisitor * v, std::string s, std::string scope) {
    return v->visit(this, s, scope);
}

std::string QDAS::WhileAction::accept(ActionVisitor * v, std::string s, std::string scope) {
    return v->visit(this, s, scope);
}

std::string QDAS::AssignFromLitAction::accept(ActionVisitor * v, std::string s, std::string scope) {
    return v->visit(this, s, scope);
}

std::string QDAS::AssignFromVarAction::accept(ActionVisitor * v, std::string s, std::string scope) {
    return v->visit(this, s, scope);
}

bool QDAS::ConcurrentQueue::isSerial() const {
    return false;
}

bool QDAS::SerialQueue::isSerial() const {
    return true;
}