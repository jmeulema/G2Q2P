G2Q2P
=====

Extraction, from a a Grand Central Dispatch code, of a QDAS model, approximation of this model, and encoding into Petri net.

In order to use this program, please do the following steps:
  - Download the source code;
  - Open the Makefile and make LIBCLANGPATH point to your libclang-3.4 instalation folder;
  - Open a terminal and enter the command "make" to compile G2Q2P;
  - Use the command "g2q2p [options] <filename>" to launch G2Q2P.

Possible options:
  - --help: Prints this help message;
  - --varmax <max>, where <max> is a natural number:
      Defines the maximum value that needs to be used to bound the variables. 
      The minimum value and default value is 1.

  The choice of the approximation:
  - --overapp: use of the over-approximation;
  - --oawlock: use of the over-approximation improved with a lock for each serial queue;
  - --underapp <qsize>, where <qsize> is a natural number: 
      Use of the under-approximation with finite queues of size <qsize>.


The precise sub-set of the C language accepted by this program is detailed in the file "INPUT-FORMAT.txt".

