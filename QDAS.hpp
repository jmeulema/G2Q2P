/* 
 * File:   QDAS.hpp
 * Author: julienmeulemans
 *
 * Created on March 15, 2014, 1:35 PM
 */

#ifndef QDAS_HPP
#define	QDAS_HPP

#include <string>
#include <list>
#include "PN.hpp"

/*
 * Queue dispatch asynchronous dispatch model representation.
 * Paper defining the formal model: 
 * Geeraerts G., Heußner A., Raskin J-F., 
 * Queue-Dispatch Asynchronous Systems, ULB Research Report, 2012
 * (http://arxiv.org/abs/1201.4871v2)
 */
class QDAS {
public:

    class ConditionVisitor;

    struct Condition {
        virtual void accept(ConditionVisitor *, std::string, std::string, std::string, std::string) = 0;
    };

    struct AlwaysTrueCond : public Condition {
        virtual void accept(ConditionVisitor *, std::string, std::string, std::string, std::string);
    };

    struct NondeterministicCond : public Condition {
        virtual void accept(ConditionVisitor *, std::string, std::string, std::string, std::string);
    };

    struct ValTestCond : public Condition {
        std::string variable;
        unsigned literal;
    };

    struct EqCond : public ValTestCond {
        virtual void accept(ConditionVisitor *, std::string, std::string, std::string, std::string);
    };

    struct NeqCond : public ValTestCond {
        virtual void accept(ConditionVisitor *, std::string, std::string, std::string, std::string);
    };

    struct LogicOpCond : public Condition {
        Condition * left;
        Condition * right;
    };

    struct ANDCond : public LogicOpCond {
        virtual void accept(ConditionVisitor *, std::string, std::string, std::string, std::string);
    };

    struct ORCond : public LogicOpCond {
        virtual void accept(ConditionVisitor *, std::string, std::string, std::string, std::string);
    };

    class ConditionVisitor {
    public:
        virtual void visit(QDAS::AlwaysTrueCond *, std::string, std::string, std::string, std::string) = 0;
        virtual void visit(QDAS::NondeterministicCond *, std::string, std::string, std::string, std::string) = 0;
        virtual void visit(QDAS::EqCond *, std::string, std::string, std::string, std::string) = 0;
        virtual void visit(QDAS::NeqCond *, std::string, std::string, std::string, std::string) = 0;
        virtual void visit(QDAS::ANDCond *, std::string, std::string, std::string, std::string) = 0;
        virtual void visit(QDAS::ORCond *, std::string, std::string, std::string, std::string) = 0;
    };

    class ActionVisitor;

    class Action {
    public:
        std::string label;
        virtual std::string accept(ActionVisitor *, std::string, std::string) = 0;
    };

    struct NullAction : public Action {
        virtual std::string accept(ActionVisitor *, std::string, std::string);
    };

    struct DispatchAction : public Action {
        std::string queue;
        std::string block;
    };

    struct DispatchAsyncAction : public DispatchAction {
        virtual std::string accept(ActionVisitor *, std::string, std::string);
    };

    struct DispatchSyncAction : public DispatchAction {
        virtual std::string accept(ActionVisitor *, std::string, std::string);
    };

    struct AssertAction : public Action {
        QDAS::Condition * cond;
        virtual std::string accept(ActionVisitor *, std::string, std::string);
    };

    struct IfAction : public Action {
        QDAS::Condition * cond;
        std::list<Action*> cons;
        std::list<Action*> alt;
        virtual std::string accept(ActionVisitor *, std::string, std::string);
    };

    struct WhileAction : public Action {
        QDAS::Condition * cond;
        std::list<Action*> core;
        virtual std::string accept(ActionVisitor *, std::string, std::string);
    };

    struct AssignAction : public Action {
        std::string dest;
    };

    struct AssignFromLitAction : public AssignAction {
        unsigned valOrg;
        virtual std::string accept(ActionVisitor *, std::string, std::string);
    };

    struct AssignFromVarAction : public AssignAction {
        std::string varOrg;
        virtual std::string accept(ActionVisitor *, std::string, std::string);
    };

    class ActionVisitor {
    public:
        virtual std::string visit(NullAction *, std::string, std::string) = 0;
        virtual std::string visit(DispatchAsyncAction *, std::string, std::string) = 0;
        virtual std::string visit(DispatchSyncAction *, std::string, std::string) = 0;
        virtual std::string visit(AssertAction *, std::string, std::string) = 0;
        virtual std::string visit(IfAction *, std::string, std::string) = 0;
        virtual std::string visit(WhileAction *, std::string, std::string) = 0;
        virtual std::string visit(AssignFromLitAction *, std::string, std::string) = 0;
        virtual std::string visit(AssignFromVarAction *, std::string, std::string) = 0;
    };

    struct Queue {
        std::string name;
        virtual bool isSerial() const = 0;
    };

    struct ConcurrentQueue : public Queue {
        bool isSerial() const;
    };

    struct SerialQueue : public Queue {
        bool isSerial() const;
    };

    struct Variable {
        std::string name;
        unsigned initval;
    };

    struct Block {
        std::string name;
        std::list<QDAS::Action*> actions;
    };



    void addQueue(Queue*);
    void addBlock(Block*);
    void setMainBlock(Block*);
    void addVariable(Variable*);
    const Queue * getQueueNamed(std::string);
    const Block * getBlockNamed(std::string);
    const std::list<Queue*>* getQueues() const;
    const std::list<Block*>* getBlocks() const;
    const std::list<Variable*>* getVars() const;
    const Block * getMainBlock() const;

private:
    std::list<Queue*> queues;
    std::list<Block*> blocks;
    Block * mainBlock;
    std::list<Variable*> vars;
};

#endif /* QDAS_HPP */

