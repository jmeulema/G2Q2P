/* 
 * File:   UAPN.cpp
 * Author: julienmeulemans
 * 
 * Created on March 25, 2014, 12:14 PM
 */

#include "UAPN.hpp"

UAPN::UAPN(unsigned max, QDAS* q, unsigned queueSize) : QPN(max, q), queueSize(queueSize) {
    addPlace("QUEUE_OVERFLOW", 0);
    addTarget("QUEUE_OVERFLOW>=1");
}

std::string UAPN::getHeaderText() {
    return "Under approximation of a QDAS, with finite serial queue of size "
    + std::to_string(queueSize) + " and variables bounded to [0," + std::to_string(maxVarVal) + "].";
}

std::string UAPN::getQueueWaitingSlotPlace(std::string queueName, unsigned nbr, bool isFree) {
    return "QWS_" + queueName + "_" + std::to_string(nbr) + "_" + (isFree ? "Free" : "Taken");
}

std::string UAPN::getBlockWaitingSlotPlace(std::string queueName, std::string blockName, unsigned nbr) {
    if (nbr == queueSize - 1) {
        return getBlockVersionNameOnSQ(blockName, queueName);
    }
    return "BWS_" + blockName + "_" + queueName + "_" + std::to_string(nbr);
}

void UAPN::visitQueue(const QDAS::Queue * q) {
    if (q->isSerial()) {
        for (unsigned i = 0; i < queueSize; ++i) {
            std::string qwsT = getQueueWaitingSlotPlace(q->name, i, true);
            std::string qwsF = getQueueWaitingSlotPlace(q->name, i, false);
            addPlace(qwsT, 1);
            addPlace(qwsF, 0);
            addInvariant(qwsT + "=1, " + qwsF + "=1");
        }
    }
}

std::string UAPN::getBlockVersionNameOnSQ(std::string blockName, std::string queueName) {
    return blockName + "_ON_" + queueName;
}

std::string UAPN::visit(QDAS::DispatchAction * a, std::string continuationPlace, std::string scope) {
    const QDAS::Queue * q = qdas->getQueueNamed(a->queue);
    if (!q->isSerial()) {
        return QPN::visit(a, continuationPlace, scope);
    }
    std::string startPlace;
    if (a->label.empty()) {
        startPlace = getUniqueScopedPlaceNameFrom("DISP", scope);
    } else {
        startPlace = a->label + "_FROM_" + scope;
    }
    addPlace(startPlace, 0);
    std::string blockVersion = getBlockVersionNameOnSQ(a->block, a->queue);
    if (!isBlockVersionAlreadyEncoded(blockVersion)) {
        visitBlock(qdas->getBlockNamed(a->block), q);
    }
    // if the queue is already full: one token is produced in QUEUE_OVERFLOW
    Transition * trans = new Transition();
    trans->guards.push_back(startPlace);
    for (unsigned i = 0; i < queueSize; ++i) {
        trans->guards.push_back(getQueueWaitingSlotPlace(q->name, i, false));
    }
    trans->incEffects.push_back("QUEUE_OVERFLOW");
    addTransition(trans);

    // if the last waiting slot is free, take it and create the new instance of the block
    trans = new Transition();
    trans->guards.push_back(startPlace);
    trans->guards.push_back(getQueueWaitingSlotPlace(q->name, queueSize - 1, true));
    trans->decEffects.push_back(startPlace);
    trans->decEffects.push_back(getQueueWaitingSlotPlace(q->name, queueSize - 1, true));
    trans->incEffects.push_back(blockVersion); //start place of the block
    trans->incEffects.push_back(getQueueWaitingSlotPlace(q->name, queueSize - 1, false));
    addTransition(trans);
    return startPlace;
}

std::string UAPN::createEndOfBlockOnSQ(std::string blockName, std::string queueName) {
    // when finished, the block needs to release the waiting slot 0.
    std::string endPlace = getUniqueScopedPlaceNameFrom("END", getBlockVersionNameOnSQ(blockName, queueName));
    addPlace(endPlace, 0);
    Transition * trans = new Transition();
    trans->guards.push_back(endPlace);
    trans->guards.push_back(getQueueWaitingSlotPlace(queueName, 0, false));
    trans->decEffects.push_back(endPlace);
    trans->incEffects.push_back(getQueueWaitingSlotPlace(queueName, 0, true));
    trans->decEffects.push_back(getQueueWaitingSlotPlace(queueName, 0, false));
    addTransition(trans);
    return endPlace;
}

void UAPN::createBlockSchedulingOnSQ(std::string startPlace, std::string beginPlace, std::string queueName, std::string blockName) {
    // create the waiting slots for this version of the block, and the transition allowing the advancement in the queue
    
    // creation of the transition symbolizing the start of the execution of the instance
    std::string waitingSlotPlace = getBlockWaitingSlotPlace(queueName, blockName, 0);
    addPlace(waitingSlotPlace, 0);
    Transition * trans = new Transition();
    trans->guards.push_back(waitingSlotPlace);
    trans->decEffects.push_back(waitingSlotPlace);
    trans->incEffects.push_back(beginPlace);
    addTransition(trans);

    for (unsigned k = 1; k < queueSize; ++k) {
        // the block waiting slot (queueSize-1) is the start place and has already be created by the "visitBlock" method from QPN
        if (k == queueSize - 1) {
            waitingSlotPlace = startPlace;
        } else {
            waitingSlotPlace = getBlockWaitingSlotPlace(queueName, blockName, k);
            addPlace(waitingSlotPlace, 0);
        }
        trans = new Transition();
        trans->guards.push_back(waitingSlotPlace);
        trans->guards.push_back(getQueueWaitingSlotPlace(queueName, k - 1, true));
        trans->guards.push_back(getQueueWaitingSlotPlace(queueName, k, false));
        trans->decEffects.push_back(waitingSlotPlace);
        trans->incEffects.push_back(getBlockWaitingSlotPlace(queueName, blockName, k - 1));
        trans->decEffects.push_back(getQueueWaitingSlotPlace(queueName, k - 1, true));
        trans->decEffects.push_back(getQueueWaitingSlotPlace(queueName, k, false));
        trans->incEffects.push_back(getQueueWaitingSlotPlace(queueName, k - 1, false));
        trans->incEffects.push_back(getQueueWaitingSlotPlace(queueName, k, true));
        addTransition(trans);
    }
}
