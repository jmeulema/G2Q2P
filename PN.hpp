/* 
 * File:   PN.hpp
 * Author: julienmeulemans
 *
 * Created on March 15, 2014, 1:36 PM
 */

#ifndef PN_HPP
#define	PN_HPP

#include <string>
#include <list>
#include <ostream>

/*
 * Place/Transition petri-net 
 *  with only arc of weight 1.
 * Can export itself into a valid input for
 * the mist tool (https://github.com/pierreganty/mist)
 */
class PN {
public:
    struct Transition {
        std::list<std::string> guards;
        std::list<std::string> incEffects;
        std::list<std::string> decEffects;
    };
    PN();
    std::string getUniquePlaceFrom(std::string);
    void addInvariant(std::string);
    void addTarget(std::string);
    void addPlace(std::string, unsigned);
    void addTransition(Transition *);
    void printForMist(std::ostream&);
protected:
    virtual std::string getHeaderText();
private:
    void printMistVars(std::ostream&);
    void printMistRules(std::ostream&);
    void printMistInit(std::ostream&);
    void printMistTargets(std::ostream&);
    void printMistInvariants(std::ostream&);

    struct Place {
        std::string name;
        unsigned initVal;
    };
    unsigned upn;
    std::list<Place *> places;
    std::list<Transition *> trans;
    std::list<std::string> invariants;
    std::list<std::string> targets;
};



#endif	/* PN_HPP */

