
#include <clang-c/Index.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <list>
#include <unistd.h>
#include <getopt.h>
#include <tuple>
#include "QDAS.hpp"
#include "OAPN.hpp"
#include "OAwLPN.hpp"
#include "UAPN.hpp"

enum CXChildVisitResult childrenCollectVisitor(CXCursor cursor, CXCursor parent, CXClientData client_data) {
    std::list<CXCursor>* csp = (std::list<CXCursor>*) client_data;
    csp->push_back(cursor);
    return CXChildVisit_Continue;
}

std::list<CXCursor> getChildren(CXCursor cursor) {
    std::list<CXCursor> cs;
    clang_visitChildren(cursor, childrenCollectVisitor, &cs);
    return cs;
}

enum CXChildVisitResult firstChildCollectVisitor(CXCursor cursor, CXCursor parent, CXClientData client_data) {
    CXCursor* csp = (CXCursor*) client_data;
    *csp = cursor;
    return CXChildVisit_Break;
}

CXCursor getFirstChild(CXCursor cursor) {
    CXCursor c;
    clang_visitChildren(cursor, firstChildCollectVisitor, &c);
    return c;
}

enum CXChildVisitResult firstLeafCollectVisitor(CXCursor cursor, CXCursor parent, CXClientData client_data) {
    CXCursor* cp = (CXCursor*) client_data;
    *cp = cursor;
    clang_visitChildren(cursor, firstLeafCollectVisitor, cp);
    return CXChildVisit_Break;
}

CXCursor getFirstLeaf(CXCursor cursor) {
    CXCursor c = cursor;
    clang_visitChildren(cursor, firstLeafCollectVisitor, &c);
    return c;
}

/*
 * FOR DEBUGING !!
void printingALL(CXCursor cursor, unsigned t) {
    CXString name = clang_getCursorSpelling(cursor);
    CXString kind = clang_getCursorKindSpelling(clang_getCursorKind(cursor));
    CXString type = clang_getTypeSpelling(clang_getCursorType(cursor));
    CXString nameString = clang_getCursorDisplayName(cursor);
    CXString usr = clang_getCursorUSR(cursor);
    for (int i = 0; i < t; ++i) {
        std::cout << "\t";
    }
    std::cout << clang_getCString(name) << ":"
            <<  clang_getCString(kind)<< ":"
            << clang_getCString(type) << "!"
            << clang_getCursorType(cursor).kind << "::"
            << clang_getCString(nameString) << "**"
            << clang_getCString(usr) << "%%";
    clang_disposeString(name);
    clang_disposeString(kind);
    clang_disposeString(type);
    clang_disposeString(nameString);
    clang_disposeString(usr);
    
    CXSourceRange range = clang_getCursorExtent(cursor);
    CXToken *tokens = 0;
    unsigned int nTokens = 0;
    CXTranslationUnit tu = clang_Cursor_getTranslationUnit(cursor);
    clang_tokenize(tu, range, &tokens, &nTokens);
    std::cout << "T:";
    for(unsigned i = 0; i < nTokens; ++i){
        CXString tokenSpelling = clang_getTokenSpelling(tu, tokens[i]);
        std::cout << clang_getCString(tokenSpelling) << " ";
        clang_disposeString(tokenSpelling);
    }
    std::cout << std::endl;
    std::list<CXCursor> cs = getChildren(cursor);
    for(std::list<CXCursor>::iterator it = cs.begin(); it != cs.end(); ++it){
        printingALL(*it, t+1);
    }
}*/

bool isSpelling(CXCursor cursor, const char* n) {
    CXString name = clang_getCursorSpelling(cursor);
    bool b = std::strcmp(clang_getCString(name), n) == 0;
    clang_disposeString(name);
    return b;
}

bool isTypeSpelling(CXCursor cursor, const char* t) {
    CXString type = clang_getTypeSpelling(clang_getCursorType(cursor));
    bool b = std::strcmp(clang_getCString(type), t) == 0;
    clang_disposeString(type);
    return b;
}

bool isBinOpAnAssign(CXCursor cursor) {
    CXSourceRange range = clang_getCursorExtent(cursor);
    CXToken *tokens = 0;
    unsigned int nTokens = 0;
    CXTranslationUnit tu = clang_Cursor_getTranslationUnit(cursor);
    clang_tokenize(tu, range, &tokens, &nTokens);
    CXString tokenSpelling = clang_getTokenSpelling(tu, tokens[1]);
    bool b = std::strcmp(clang_getCString(tokenSpelling), "=") == 0;
    clang_disposeString(tokenSpelling);
    return b;
}

std::list<std::string> getTokens(CXCursor cursor) {
    std::list<std::string> l;
    CXSourceRange range = clang_getCursorExtent(cursor);
    CXToken *tokens = 0;
    unsigned int nTokens = 0;
    CXTranslationUnit tu = clang_Cursor_getTranslationUnit(cursor);
    clang_tokenize(tu, range, &tokens, &nTokens);
    for (unsigned i = 0; i < nTokens; ++i) {
        CXString tokenSpelling = clang_getTokenSpelling(tu, tokens[i]);
        l.push_back(clang_getCString(tokenSpelling));
        clang_disposeString(tokenSpelling);
    }
    return l;
}

void handleError(CXCursor cursor, const char* org) {
    CXString name = clang_getCursorSpelling(cursor);
    CXString kind = clang_getCursorKindSpelling(clang_getCursorKind(cursor));
    CXSourceLocation location = clang_getCursorLocation(cursor);
    unsigned line, column;
    clang_getPresumedLocation(location, 0, &line, &column);
    std::cerr << "Error on '" << clang_getCString(name) << "'["
            << line << "," << column << "] : "
            << org << ", type:" << clang_getCString(kind);
    clang_disposeString(name);
    clang_disposeString(kind);

    // print the original text(from the source) where the error comes from.
    CXSourceRange range = clang_getCursorExtent(cursor);
    CXToken *tokens = 0;
    unsigned int nTokens = 0;
    CXTranslationUnit tu = clang_Cursor_getTranslationUnit(cursor);
    clang_tokenize(tu, range, &tokens, &nTokens);
    std::cout << ", Tokens '";
    for (unsigned i = 0; i < nTokens; ++i) {
        CXString tokenSpelling = clang_getTokenSpelling(tu, tokens[i]);
        std::cout << clang_getCString(tokenSpelling) << " ";
        clang_disposeString(tokenSpelling);
    }
    std::cout << "'" << std::endl;
    exit(-1);
}

int extractIntLiteral(CXCursor cursor) {
    CXSourceRange range = clang_getCursorExtent(cursor);
    CXToken *tokens = 0;
    unsigned int nTokens = 0;
    CXTranslationUnit tu = clang_Cursor_getTranslationUnit(cursor);
    clang_tokenize(tu, range, &tokens, &nTokens);
    CXString tokenSpelling = clang_getTokenSpelling(tu, tokens[0]);
    int i = atoi(clang_getCString(tokenSpelling));
    clang_disposeString(tokenSpelling);
    return i;
}

QDAS::AssignAction * extractAssignment(CXCursor cursor) {
    QDAS::AssignAction * a = NULL;
    std::list<CXCursor> cs = getChildren(cursor);

    // check kind of the left hand side and extract assignee name
    CXCursor c = cs.front();
    cs.pop_front();
    enum CXCursorKind kind = clang_getCursorKind(c);
    if (kind != CXCursor_DeclRefExpr) {
        handleError(c, "Assignment varRef extraction");
    }
    CXString name = clang_getCursorSpelling(c);

    // extract assignation source: literal or variable
    c = cs.front();
    cs.pop_front();
    kind = clang_getCursorKind(c);
    if (kind == CXCursor_UnexposedExpr) {
        c = getFirstChild(c);
        kind = clang_getCursorKind(c);
    }
    if (kind == CXCursor_IntegerLiteral) {
        QDAS::AssignFromLitAction * l = new QDAS::AssignFromLitAction();
        l->valOrg = extractIntLiteral(c);
        a = l;
    } else if (kind == CXCursor_DeclRefExpr) {
        QDAS::AssignFromVarAction * v = new QDAS::AssignFromVarAction();
        CXString name = clang_getCursorSpelling(c);
        v->varOrg = clang_getCString(name);
        clang_disposeString(name);
        a = v;
    } else {
        handleError(c, "Assignment LHS extraction");
    }
    a->dest = clang_getCString(name);
    clang_disposeString(name);
    return a;
}

void extractValTestCond(CXCursor cursor, QDAS::ValTestCond& v) {
    // extract LHS and RHS of an equality or inequality test
    std::list<CXCursor> cs = getChildren(cursor);
    cursor = cs.front();
    enum CXCursorKind kind = clang_getCursorKind(cursor);
    if (kind == CXCursor_UnexposedExpr) {
        cursor = getFirstChild(cursor);
        kind = clang_getCursorKind(cursor);
    }
    if (kind != CXCursor_DeclRefExpr) {
        handleError(cursor, "ValTest var extraction");
    }
    CXString name = clang_getCursorSpelling(cursor);
    v.variable = clang_getCString(name);
    clang_disposeString(name);
    v.literal = extractIntLiteral(cs.back());
}

std::string extractBinaryOpOfCondition(CXCursor cursor) {
    /* if cursor is a simple condition:
     *          -> the operator is in second place : 
     *             Tokens: v0 == 3 )
     *                     ___^_____
     * if cursor is a compound condition:
     *          -> the operator is the last token of the LHS of this compound condition
     *              v0 == 3 && v1 != 9 )
     *              ________^_ ---------
     */
    std::list<std::string> tokens = getTokens(cursor);
    if (tokens.size() == 4) {
        return *(++(tokens.begin())); // return the second
    }
    tokens = getTokens(getFirstChild(cursor));
    return tokens.back();
}

void extractCompoundActionTo(CXCursor, std::list<QDAS::Action*>&);

QDAS::Condition * extractCondition(CXCursor cursor) {
    QDAS::Condition * cond = NULL;
    enum CXCursorKind kind = clang_getCursorKind(cursor);
    // skip the implicit casts and the parentheses
    if (kind == CXCursor_UnexposedExpr || kind == CXCursor_ParenExpr) {
        cursor = getFirstChild(cursor);
        kind = clang_getCursorKind(cursor);
    }
    //  "if(v)" is converted to "if(v != 0)"
    if (kind == CXCursor_DeclRefExpr) {
        CXString name = clang_getCursorSpelling(cursor);
        std::string variable = clang_getCString(name);
        if (variable == "NONDETERMINISTIC") {
            cond = new QDAS::NondeterministicCond();
        } else {
            QDAS::NeqCond * neqc = new QDAS::NeqCond();
            neqc->variable = variable;
            neqc->literal = 0;
            clang_disposeString(name);
            cond = neqc;
        }
    } else if (kind == CXCursor_IntegerLiteral) {
        if (1 != extractIntLiteral(cursor)) {
            handleError(cursor, "Condition invalid literal");
        } else {
            cond = new QDAS::AlwaysTrueCond();
        }
    } else if (kind == CXCursor_BinaryOperator) {
        std::string op = extractBinaryOpOfCondition(cursor);
        if ("==" == op) {
            QDAS::ValTestCond * v = new QDAS::EqCond();
            extractValTestCond(cursor, *v);
            cond = v;
        } else if ("!=" == op) {
            QDAS::ValTestCond * v = new QDAS::NeqCond();
            extractValTestCond(cursor, *v);
            cond = v;
        } else if ("&&" == op) {
            std::list<CXCursor> cs = getChildren(cursor);
            QDAS::ANDCond * ac = new QDAS::ANDCond();
            ac->left = extractCondition(cs.front());
            ac->right = extractCondition(cs.back());
            cond = ac;
        } else if ("||" == op) {
            std::list<CXCursor> cs = getChildren(cursor);
            QDAS::ORCond * oc = new QDAS::ORCond();
            oc->left = extractCondition(cs.front());
            oc->right = extractCondition(cs.back());
            cond = oc;
        } else {
            handleError(cursor, "Condition invalid operator");
        }
    } else {
        handleError(cursor, "Invalid condition form");
    }
    return cond;
}

QDAS::IfAction* extractIfAction(CXCursor cursor) {
    QDAS::IfAction * a = new QDAS::IfAction();
    std::list<CXCursor> cs = getChildren(cursor);

    CXCursor c = cs.front();
    cs.pop_front();
    a->cond = extractCondition(c);

    c = cs.front();
    cs.pop_front();
    enum CXCursorKind kind = clang_getCursorKind(c);
    if (kind != CXCursor_CompoundStmt) {
        handleError(c, "IfAction consequence extraction");
    }
    extractCompoundActionTo(c, a->cons);
    
    if (!cs.empty()) {
        c = cs.front();
        kind = clang_getCursorKind(c);
        if (kind != CXCursor_CompoundStmt && kind != CXCursor_IfStmt) {
            handleError(c, "IfAction alternative extraction");
        }
        if(kind == CXCursor_CompoundStmt){
            extractCompoundActionTo(c, a->alt);
        } else { // handle the "else if" case 
            a->alt.push_back(extractIfAction(c));
        }
    }
    return a;
}

QDAS::WhileAction* extractWhileAction(CXCursor cursor) {
    QDAS::WhileAction * a = new QDAS::WhileAction();
    std::list<CXCursor> cs = getChildren(cursor);

    CXCursor c = cs.front();
    cs.pop_front();
    a->cond = extractCondition(c);

    c = cs.front();
    cs.pop_front();
    enum CXCursorKind kind = clang_getCursorKind(c);
    if (kind != CXCursor_CompoundStmt) {
        handleError(c, "WhileAction core extraction");
    }
    extractCompoundActionTo(c, a->core);
    return a;
}

std::string extractDispatchedQueue(CXCursor cursor) {
    CXCursor cQueue = clang_Cursor_getArgument(cursor, 0);
    CXString nameQueue = clang_getCursorSpelling(cQueue);
    std::string name(clang_getCString(nameQueue));
    clang_disposeString(nameQueue);
    return name;
}

std::string extractDispatchedBlock(CXCursor cursor) {
    CXCursor cBlock = clang_Cursor_getArgument(cursor, 1);
    CXString nameBlock = clang_getCursorSpelling(cBlock);
    std::string name(clang_getCString(nameBlock));
    clang_disposeString(nameBlock);
    return name;
}

QDAS::Action * extractAction(CXCursor cursor) {
    enum CXCursorKind kind = clang_getCursorKind(cursor);
    QDAS::Action * a = NULL;
    if (kind == CXCursor_LabelStmt) {
        CXString label = clang_getCursorSpelling(cursor);
        a = extractAction(getFirstChild(cursor));
        a->label = clang_getCString(label);
        clang_disposeString(label);
    } else if (kind == CXCursor_NullStmt) {
        a = new QDAS::NullAction();
    } else if (kind == CXCursor_IfStmt) {
        a = extractIfAction(cursor);
    } else if (kind == CXCursor_WhileStmt) {
        a = extractWhileAction(cursor);
    } else if (kind == CXCursor_BinaryOperator && isBinOpAnAssign(cursor)) {
        a = extractAssignment(cursor);
    } else if (kind == CXCursor_CallExpr) {
        if (isSpelling(cursor, "assert")) {
            if (clang_Cursor_getNumArguments(cursor) != 1) {
                handleError(cursor, "Incorrect assert param #");
            }
            QDAS::AssertAction * as = new QDAS::AssertAction();
            as->cond = extractCondition(clang_Cursor_getArgument(cursor, 0));
            a = as;
        } else {
            QDAS::DispatchAction * d = NULL;
            if (clang_Cursor_getNumArguments(cursor) != 2) {
                handleError(cursor, "Incorrect dispatch param #");
            }
            if (isSpelling(cursor, "dispatch_async")) {
                d = new QDAS::DispatchAsyncAction();
            } else if (isSpelling(cursor, "dispatch_sync")) {
                d = new QDAS::DispatchSyncAction();
            } else {
                handleError(cursor, "Invalid call");
            }
            d->queue = extractDispatchedQueue(cursor);
            d->block = extractDispatchedBlock(cursor);
            a = d;
        }
    } else {
        handleError(cursor, "Statement extraction");
    }
    return a;
}

void extractCompoundActionTo(CXCursor cursor, std::list<QDAS::Action*>& dst) {
    std::list<CXCursor> cs = getChildren(cursor);
    for (std::list<CXCursor>::iterator it = cs.begin(); it != cs.end(); ++it) {
        dst.push_back(extractAction(*it));
    }
}

QDAS::Block * extractBlockDecl(CXCursor cursor) {
    QDAS::Block * b = new QDAS::Block();
    CXString name = clang_getCursorSpelling(cursor);
    b->name = clang_getCString(name);
    clang_disposeString(name);

    CXCursor c = getChildren(cursor).back();
    if (clang_getCursorKind(c) == CXCursor_UnexposedExpr) {
        c = getFirstChild(c);
    }
    if (clang_getCursorKind(c) != CXCursor_BlockExpr) {
        handleError(c, "Block definition extraction");
    }
    c = getFirstChild(c);
    if (clang_getCursorKind(c) != CXCursor_UnexposedDecl) {
        handleError(c, "Block definition extraction: missing BlockDecl.");
    }
    c = getFirstChild(c);
    if (clang_getCursorKind(c) != CXCursor_CompoundStmt) {
        handleError(c, "Block definition extraction: missing compoundStmt.");
    }
    extractCompoundActionTo(c, b->actions);
    return b;
}

QDAS::Queue * extractQueueDecl(CXCursor cursor) {
    QDAS::Queue * q = NULL;
    CXString name = clang_getCursorSpelling(cursor);
    CXCursor c = getChildren(cursor).back();
    if (clang_getCursorKind(c) != CXCursor_CallExpr
            || !isSpelling(c, "dispatch_queue_create")) {
        handleError(c, "QueueInit LHS extraction");
    }
    if (clang_Cursor_getNumArguments(c) != 2) {
        handleError(c, "Incorrect dispatch_queue_create param #");
    }
    c = getFirstLeaf(clang_Cursor_getArgument(c, 1));
    if (clang_getCursorKind(c) == CXCursor_IntegerLiteral) {
        q = new QDAS::SerialQueue();
    } else if (clang_getCursorKind(c) == CXCursor_DeclRefExpr && isSpelling(c, "_dispatch_queue_attr_concurrent")) {
        q = new QDAS::ConcurrentQueue();
    } else {
        handleError(c, "Queue type extraction");
    }
    q->name = clang_getCString(name);
    clang_disposeString(name);
    return q;
}

/* in the main:
    - queues decls
    - <dispatch_retain(queue)> // optional 
    - block decls
    - ... actions ...
    - <dispatch_release(queue)> // optional 
    - return 0
 */
void handleMainBlock(CXCursor cursor, QDAS& qdas) {
    CXCursor c = getFirstChild(cursor);
    enum CXCursorKind kind = clang_getCursorKind(c);
    if (kind != CXCursor_CompoundStmt) {
        handleError(c, "Main core extraction");
    }
    std::list<CXCursor> cs = getChildren(c);
    std::list<CXCursor>::iterator it = cs.begin();
    // extract the queues' declaration
    for (CXCursor child = getFirstChild(*it);
            it != cs.end()
            && clang_getCursorKind(*it) == CXCursor_DeclStmt
            && clang_getCursorKind(child) == CXCursor_VarDecl
            && isTypeSpelling(child, "dispatch_queue_t");
            ++it, child = getFirstChild(*it)) {
        qdas.addQueue(extractQueueDecl(child));
    }
    // skip the "dispatch_retain"
    for (; it != cs.end()
            && clang_getCursorKind(*it) == CXCursor_CallExpr
            && isSpelling(*it, "dispatch_retain");
            ++it);
    // extract the blocks' declaration
    if (it != cs.end()) {
        CXCursor child = getFirstChild(*it);
        while (it != cs.end()
                && clang_getCursorKind(*it) == CXCursor_DeclStmt
                && clang_getCursorKind(child) == CXCursor_VarDecl
                && isTypeSpelling(child, "dispatch_block_t")) {
            qdas.addBlock(extractBlockDecl(child));
            ++it;
            child = getFirstChild(*it);
        }
    } else {
        handleError(cursor, "Main end before block defs");
    }

    // extract actions of the main block
    QDAS::Block * mb = new QDAS::Block();
    mb->name = "MAIN";
    for (; it != cs.end()
            && clang_getCursorKind(*it) != CXCursor_ReturnStmt
            && !(clang_getCursorKind(*it) == CXCursor_CallExpr
            && isSpelling(*it, "dispatch_release"));
            ++it) {
        mb->actions.push_back(extractAction(*it));
    }
    if (mb->actions.empty()) {
        if (it == cs.end()) {
            handleError(cursor, "Main premature end");
        }
        handleError(*it, "Main actions extraction");
    }
    qdas.setMainBlock(mb);

    // skip the "dispatch_retain"
    for (; it != cs.end()
            && clang_getCursorKind(*it) == CXCursor_CallExpr
            && isSpelling(*it, "dispatch_release");
            ++it);
}

QDAS::Variable * extractVarDecl(CXCursor cursor) {
    QDAS::Variable * v = new QDAS::Variable();
    CXString name = clang_getCursorSpelling(cursor);
    v->name = clang_getCString(name);
    clang_disposeString(name);
    CXCursor c = getFirstChild(cursor);
    // ignore the implicit casts
    if (clang_getCursorKind(c) == CXCursor_UnexposedExpr) {
        c = getFirstChild(c);
    }
    if (clang_getCursorKind(c) == CXCursor_IntegerLiteral) {
        v->initval = extractIntLiteral(c);
    }
    return v;
}

QDAS* parseASTtoQDAS(CXCursor cursor) {
    QDAS * qdas = new QDAS();
    std::list<CXCursor> cs = getChildren(cursor);
    for (std::list<CXCursor>::iterator it = cs.begin(); it != cs.end(); ++it) {
        CXSourceLocation location = clang_getCursorLocation(*it);
        if (clang_Location_isFromMainFile(location)) {
            enum CXCursorKind kind = clang_getCursorKind(*it);
            if (kind == CXCursor_VarDecl && clang_getCursorType(*it).kind == CXType_Int) {
                qdas->addVariable(extractVarDecl(*it));
            } else if (kind == CXCursor_FunctionDecl && isSpelling(*it, "main")) {
                handleMainBlock(*it, *qdas);
            }
        }
    }
    return qdas;
}

void print_help() {
    std::cout << "Usage: g2q2p [options] <filename>" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "\t--help    \t\t this help message" << std::endl;
    std::cout << "\t--varmax <max> \t\t limit the variables from 0 to <max> (default is 1)" << std::endl;

    std::cout << "  Choose the approximation method for the QDAS" << std::endl;
    std::cout << "\t--overapp \t\t use an over approximation" << std::endl;
    std::cout << "\t--oawlock \t\t use an over approximation with a lock" << std::endl;
    std::cout << "\t--underapp <qsize> \t use an under approximation with finite serial queues of size <queuesize>" << std::endl << std::endl;
}

std::tuple<unsigned, unsigned, unsigned> extractCommandLineOptions(int argc, char *const *argv) {
    int c;
    int approxType = 0;
    unsigned qsize = 1;
    unsigned varmax = 1;

    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"help", no_argument, 0, 'h'},
            {"varmax", required_argument, 0, 'v'},
            {"overapp", no_argument, &approxType, 0},
            {"oawlock", no_argument, &approxType, 1},
            {"underapp", required_argument, 0, 'u'},
            {0, 0, 0, 0}
        };

        c = getopt_long(argc, argv, "hu:v:", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
            case 0:
                break;
            case 'h':
                print_help();
                exit(EXIT_SUCCESS);
                break;
            case 'v':
                if (atoi(optarg) == 0) {
                    print_help();
                    std::cerr << "---- --varmax option needs an argument ----" << std::endl;
                    exit(EXIT_FAILURE);
                }
                varmax = atoi(optarg);
                break;
            case 'u':
                approxType = 2;
                if (atoi(optarg) == 0) {
                    print_help();
                    std::cerr << "---- --underapp option needs an argument ----" << std::endl;
                    exit(EXIT_FAILURE);
                }
                qsize = atoi(optarg);
                break;
            default:
                print_help();
                std::cerr << "?? getopt returned character code " << c << " ??" << std::endl;
                exit(EXIT_FAILURE);
        }
    }

    if (optind >= argc) {
        print_help();
        std::cerr << "---- Missing filename ----" << std::endl;
        exit(EXIT_FAILURE);
    }
    return std::make_tuple(approxType, varmax, qsize);
}

int main(int argc, char *const *argv) {
    std::tuple<char, unsigned, unsigned> opt = extractCommandLineOptions(argc, argv);
    std::string filename = argv[optind++];
    CXIndex index = clang_createIndex(0, 0);
    if (index == 0) {
        std::cerr << "Error creating the index!" << std::endl;
        return 1;
    }
    CXTranslationUnit tu = clang_parseTranslationUnit(index, filename.c_str(), 0, 0, 0, 0, CXTranslationUnit_None);
    if (tu == 0) {
        std::cerr << "Error creating the translation unit!" << std::endl;
        return 1;
    }
    unsigned nbDiag = clang_getNumDiagnostics(tu);

    // if the code contains errors, print them
    for (unsigned i = 0; i != nbDiag; ++i) {
        CXDiagnostic diag = clang_getDiagnostic(tu, i);
        CXString diag_str = clang_formatDiagnostic(diag, clang_defaultDiagnosticDisplayOptions());
        fprintf(stderr, "%s\n", clang_getCString(diag_str));
        clang_disposeString(diag_str);
    }
    
    if (nbDiag == 0) { // if no errors during the AST construction
        std::cout << "Analyze of code: " << clang_getCString(clang_getTranslationUnitSpelling(tu)) << std::endl;
        std::cout << "Extraction of the QDAS..." << std::endl;
        QDAS * qdas = parseASTtoQDAS(clang_getTranslationUnitCursor(tu));
        QPN * pn;
        if (std::get<0>(opt) == 0) {
            pn = new OAPN(std::get<1>(opt), qdas);
            std::cout << "Over approximation of the QDAS..." << std::endl;
        } else if (std::get<0>(opt) == 1) {
            pn = new OAwLPN(std::get<1>(opt), qdas);
            std::cout << "Over approximation with lock of the QDAS..." << std::endl;
        } else if (std::get<0>(opt) == 2) {
            pn = new UAPN(std::get<1>(opt), qdas, std::get<2>(opt));
            std::cout << "Under approximation of the QDAS..." << std::endl;
        } else {
            std::cerr << "Unexpected failure" << std::endl;
            exit(EXIT_FAILURE);
        }

        pn->build();
        std::cout << "Exporting the PN..." << std::endl;
        std::string str(filename);
        str = str.substr(0, str.find_first_of('.')) + ".spec";
        std::ofstream ofs(str, std::ofstream::out);
        pn->printForMist(ofs);
        ofs.close();
        std::cout << "Export successful to '" << str << "'" << std::endl;
    }
    clang_disposeTranslationUnit(tu);
    clang_disposeIndex(index);
    return 0;
}


