/* 
 * File:   OAwLPN.cpp
 * Author: julienmeulemans
 * 
 * Created on March 25, 2014, 10:41 AM
 */

#include "OAwLPN.hpp"

OAwLPN::OAwLPN(unsigned max, QDAS* q) : QPN(max, q) {
}

std::string OAwLPN::getHeaderText(){
    return "Over approximation of a QDAS, with serial queues abstracted with locks and variables bounded to [0," + std::to_string(maxVarVal) + "].";
}

void OAwLPN::visitQueue(const QDAS::Queue * q) {
    if (q->isSerial()) {
        addPlace(getLockPlaceOf(q->name), 1);
    }
}

std::string OAwLPN::getLockPlaceOf(std::string queueName) {
    return "LOCK_FOR_" + queueName;
}

std::string OAwLPN::getBlockVersionNameOnSQ(std::string blockName, std::string queueName) {
    return blockName + "_ON_" + queueName;
}


std::string OAwLPN::createEndOfBlockOnSQ(std::string blockName, std::string queueName) {
    // at the end of a block, the lock (of the queue he was in) needs to be released
    std::string endPlace = getUniqueScopedPlaceNameFrom("END", getBlockVersionNameOnSQ(blockName, queueName));
    addPlace(endPlace, 0);
    Transition * trans = new Transition();
    trans->guards.push_back(endPlace);
    trans->decEffects.push_back(endPlace);
    trans->incEffects.push_back(getLockPlaceOf(queueName));
    addTransition(trans);
    return endPlace;
}

void OAwLPN::createBlockSchedulingOnSQ(std::string startPlace, std::string beginPlace, std::string queueName, std::string blockName) {
    // in order to start, a block needs to acquire the lock of the queue he is in
    Transition * trans = new Transition();
    trans->guards.push_back(startPlace);
    trans->guards.push_back(getLockPlaceOf(queueName));
    trans->decEffects.push_back(startPlace);
    trans->decEffects.push_back(getLockPlaceOf(queueName));
    trans->incEffects.push_back(beginPlace);
    addTransition(trans);
}


