CXX = clang++ -std=c++11
# change LIBCLANGPATH to point to the libclang library folder
LIBCLANGPATH = ../libclang-3.4
INCLUDES = -I$(LIBCLANGPATH)/include/
LFLAGS =  -L$(LIBCLANGPATH)/lib/
LIBS = -lclang

# define the C source files
SRCS = OAPN.cpp OAwLPN.cpp PN.cpp QDAS.cpp QPN.cpp UAPN.cpp main.cpp


OBJS = $(SRCS:.cpp=.o)

MAIN = g2q2p


all:    $(MAIN)
		@echo  G2Q2P has been compiled

$(MAIN): $(OBJS) 
		$(CXX) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)


.c.o:
		$(CXX) $(INCLUDES) -c $<  -o $@

clean:
		$(RM) *.o *~ $(MAIN)

