/* 
 * File:   OAwLPN.hpp
 * Author: julienmeulemans
 *
 * Created on March 25, 2014, 10:40 AM
 */

#ifndef OAWLPN_HPP
#define	OAWLPN_HPP
#include "QPN.hpp"
#include "QDAS.hpp"

/*
 * Concrete over-approximation of a QDAS into a PetriNet
 *  by abstracting the mutual exclusion of serial queues with 
 *  a lock, and all dispatches by asynchronous ones.
 */
class OAwLPN : public QPN {
public:
    OAwLPN(unsigned, QDAS *);
protected:
    virtual std::string getHeaderText();
    virtual void visitQueue(const QDAS::Queue *);
    std::string getLockPlaceOf(std::string);
    virtual std::string getBlockVersionNameOnSQ(std::string, std::string);
    virtual std::string createEndOfBlockOnSQ(std::string, std::string);
    virtual void createBlockSchedulingOnSQ(std::string, std::string, std::string, std::string);
};

#endif	/* OAWLPN_HPP */

